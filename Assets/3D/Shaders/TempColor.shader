Shader "Hidden/TempColor"
{
    Properties
    {

    }
    SubShader
    {

        Cull Back ZWrite Off ZTest Always
        // Stencil
        // {
        //     Ref 2
        //     Comp Always
        //     Pass replace
        // }
        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/DeclareDepthTexture.hlsl"
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD1;
                float3 worldPos : TEXCOORD2;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = TransformObjectToHClip(v.vertex);
                o.uv = v.uv;
                o.screenPos = ComputeScreenPos(o.vertex );
                o.worldPos = TransformObjectToWorld(v.vertex);
                return o;
            }


            float4 frag (v2f i) : SV_Target
            {
                float depth= SampleSceneDepth(i.screenPos.xy / i.screenPos.w);
                float3 A=(  i.worldPos-_WorldSpaceCameraPos);
                // float3 B = unity_CameraToWorld._m02_m12_m22;
                // float3 B= mul((float3x3)unity_CameraToWorld,float3(0,0,1));
                float3 B= -1 * mul(UNITY_MATRIX_M, transpose(mul(UNITY_MATRIX_I_M, UNITY_MATRIX_I_V)) [2].xyz);
                float depth2 = length (B * dot(A, B) / dot(B, B))/_ProjectionParams.z;
                depth=Linear01Depth(depth, _ZBufferParams);
               
                if(depth2-depth<0.0001f){
                     return (1,1,1,1);
                }
                    return (0,0,0,0);
       
            }
            ENDHLSL
        }
    }
}
