Shader "Hidden/BlurShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            Name "Blur Horizontal"
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = TransformObjectToHClip(v.vertex);
                o.uv = v.uv;
                return o;
            }
            TEXTURE2D(_MainTex);
            SAMPLER(sampler_MainTex);
float4 _MainTex_TexelSize;
            float _Thiccness;
            half4 frag (v2f i) : SV_Target
            {
                half4 sum = 0;
                int samples = 4;
                // _Thiccness*=0.01f;
                for (float x = -samples; x < samples; x++)
                {
                    float2 offset = float2(x * _Thiccness, 0);
                    sum += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.uv + offset*_MainTex_TexelSize.xy);
                }
      
                return sum /(samples*2);
            }
            ENDHLSL
        }
        Pass
        {
            Name "Blur Vertical"
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = TransformObjectToHClip(v.vertex);
                o.uv = v.uv;
                return o;
            }
            TEXTURE2D(_MainTex);
            SAMPLER(sampler_MainTex);
float4 _MainTex_TexelSize;
            float _Thiccness;
            half4 frag (v2f i) : SV_Target
            {
                half4 sum = 0;
                int samples = 5;
                // _Thiccness*=0.01f;
                for (float y = -samples; y < samples; y++)
                {
                    float2 offset = float2( 0,y * _Thiccness);
                    sum += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.uv + offset*_MainTex_TexelSize.xy);
                }
      
                return sum /(samples*2);
            }
            ENDHLSL
        }
        Pass
        {
            Name "Composite"
            // Stencil
            // {
            //     Ref 2
            //     Comp NotEqual
        
            // }
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = TransformObjectToHClip(v.vertex);
                o.uv = v.uv;
                return o;
            }
            TEXTURE2D(_MainTex);
            SAMPLER(sampler_MainTex);
             TEXTURE2D(_CameraOpaqueTexture);
            SAMPLER(sampler_CameraOpaqueTexture);
             TEXTURE2D(_MaskTexture);
            SAMPLER(sampler_MaskTexture);
float4 _Color;
            half4 frag (v2f i) : SV_Target
            {
             float4 color =SAMPLE_TEXTURE2D(_CameraOpaqueTexture, sampler_CameraOpaqueTexture, i.uv);
             half mask = SAMPLE_TEXTURE2D(_MaskTexture, sampler_MaskTexture, i.uv).r;
             half outline =SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.uv).r;
             if(mask<0.5f){
                 color=color + outline *_Color;
             }
            return color;
            // return SAMPLE_TEXTURE2D(_CameraOpaqueTexture, sampler_CameraOpaqueTexture, i.uv) ;
            }
            ENDHLSL
        }
    }
}
