using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    [HideInInspector]
    public GameObject desiredCanvas;
    public GameObject[] canvas;
    private bool read;

    // Start is called before the first frame update
    void Start()
    {
        MainMenu();
    }

    public void MainMenu()
    {
        for (int i = 0; i < canvas.Length; i++)
        {
            canvas[i].SetActive(false);
        }
        canvas[0].SetActive(true);
    }


    public void SettingsMenu()
    {
        for (int i = 0; i < canvas.Length; i++)
        {
            canvas[i].SetActive(false);
        }
        canvas[1].SetActive(true);
    }


    public void RevisionMenu()
    {
        for (int i = 0; i < canvas.Length; i++)
        {
            canvas[i].SetActive(false);
        }
        canvas[2].SetActive(true);
    }
}
