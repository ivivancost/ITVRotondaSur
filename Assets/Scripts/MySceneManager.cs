using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour
{
    public string Level;
    // public Animator animFade;


    public void ChangeScene()
    {
        SceneManager.LoadScene(Level, LoadSceneMode.Single);
    }



    public void QuitApp()
    {
        Application.Quit();
        Debug.Log("salgo");
    }



    // public void FadeOutScene()
    // {
    //     animFade.SetTrigger("FadeOut");
    // }

    // public void FadeOuttoMain()
    // {
    //     animFade.SetTrigger("FadeToMenu");
    // }


}
