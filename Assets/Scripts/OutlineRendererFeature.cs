using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

internal class OutlineRendererFeature : ScriptableRendererFeature
{
    [System.Serializable]
    public class Settings
    {
        public float thicc;
        [ColorUsage(true, true)] public Color color;
        public Material colorMaterial;
        public Material blurMaterial;
        public RenderPassEvent renderPass;
        public LayerMask filter;
        public enum Downsample
        {
            Full = 1,
            Half = 2,
            Third = 3,
            Quarter = 4
        }
        public Downsample downsampling = Downsample.Full;
    }

    public Settings settings;

    OutlinePass m_RenderPass = null;

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        if (renderingData.cameraData.cameraType == CameraType.Game)
        {
            //Calling ConfigureInput with the ScriptableRenderPassInput.Color argument ensures that the opaque texture is available to the Render Pass
            m_RenderPass.ConfigureInput(ScriptableRenderPassInput.Color);
            m_RenderPass.SetTarget(renderer.cameraColorTarget, 1);
            renderer.EnqueuePass(m_RenderPass);
        }
    }

    public override void Create()
    {
        m_RenderPass = new OutlinePass(settings);
    }

    protected override void Dispose(bool disposing)
    {

    }

    internal class OutlinePass : ScriptableRenderPass
    {
        ProfilingSampler m_ProfilingSampler = new ProfilingSampler("Outline");
        Material material;
        RenderTargetIdentifier m_CameraColorTarget;
        float m_Intensity;
        Settings _settings;
        private RenderTargetHandle renderTarget;
        private RenderTargetHandle renderTarget2;

        List<ShaderTagId> m_ShaderTagIdList = new List<ShaderTagId>();

        public OutlinePass(Settings settings)
        {
            material = settings.colorMaterial;
            _settings = settings;
            renderPassEvent = settings.renderPass;
            m_ShaderTagIdList.Add(new ShaderTagId("SRPDefaultUnlit"));
            m_ShaderTagIdList.Add(new ShaderTagId("UniversalForward"));
            m_ShaderTagIdList.Add(new ShaderTagId("UniversalForwardOnly"));
            m_ShaderTagIdList.Add(new ShaderTagId("LightweightForward"));

        }

        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            cameraTextureDescriptor.width /= (int)_settings.downsampling;
            cameraTextureDescriptor.height /= (int)_settings.downsampling;
            cameraTextureDescriptor.depthBufferBits = 0;
            //we dont need to resolve AA in every single Blit
            cameraTextureDescriptor.msaaSamples = 1;
            cameraTextureDescriptor.graphicsFormat = GraphicsFormat.R16_UNorm;
            renderTarget.id = 53;
            renderTarget2.id = 54;
            cmd.GetTemporaryRT(renderTarget.id, cameraTextureDescriptor);
            cmd.GetTemporaryRT(renderTarget2.id, cameraTextureDescriptor);
            ConfigureTarget(renderTarget.Identifier());
            ConfigureClear(ClearFlag.All, Color.clear);
        }

        public void SetTarget(RenderTargetIdentifier colorHandle, float intensity)
        {
            m_CameraColorTarget = colorHandle;
            m_Intensity = intensity;
        }

        public override void OnCameraSetup(CommandBuffer cmd, ref RenderingData renderingData)
        {
            ConfigureTarget(new RenderTargetIdentifier(m_CameraColorTarget, 0, CubemapFace.Unknown, -1));
        }

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            var camera = renderingData.cameraData.camera;
            // if (camera.cameraType != CameraType.Game)
            //     return;

            if (material == null || _settings.blurMaterial == null)
                return;

            SortingCriteria sortingCriteria = SortingCriteria.CommonTransparent;
            DrawingSettings drawingSettings = CreateDrawingSettings(m_ShaderTagIdList, ref renderingData, sortingCriteria);
            drawingSettings.overrideMaterial = material;
            FilteringSettings filteringSettings = new FilteringSettings(RenderQueueRange.opaque, _settings.filter);

            CommandBuffer cmd = CommandBufferPool.Get();
            using (new ProfilingScope(cmd, m_ProfilingSampler))
            {
                //mascara
                cmd.SetRenderTarget(renderTarget.Identifier());
                context.DrawRenderers(renderingData.cullResults, ref drawingSettings, ref filteringSettings);
                _settings.blurMaterial.SetFloat("_Thiccness", _settings.thicc);
                cmd.SetGlobalTexture("_MaskTexture", renderTarget.Identifier());
                //blur horizontal
                cmd.Blit(renderTarget.Identifier(), renderTarget2.Identifier(), _settings.blurMaterial, 0);

                //blur vertical
                cmd.Blit(renderTarget2.Identifier(), renderTarget.Identifier(), _settings.blurMaterial, 1);

                // cmd.SetGlobalTexture("_ColorTexture", m_CameraColorTarget);
                // //composite
                _settings.blurMaterial.SetColor("_Color", _settings.color);
                cmd.Blit(renderTarget.Identifier(), m_CameraColorTarget, _settings.blurMaterial, 2);

            }

            context.ExecuteCommandBuffer(cmd);
            cmd.Clear();
            CommandBufferPool.Release(cmd);
        }
    }
}