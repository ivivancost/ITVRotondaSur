using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalculoFechaRevision : MonoBehaviour
{
    public Dropdown dropdown;
    public Text infoCalculoText;
    public Text fechaText;
    private int temp;
    private int fecha;

    public void DisplayAnswer()
    {
        StartCoroutine(CalculateAndDisplay());
    }

    IEnumerator CalculateAndDisplay()
    {
        yield return null;
        infoCalculoText.text = "Calculando.";
        yield return new WaitForSeconds(0.40f);
        infoCalculoText.text = "Calculando..";
        yield return new WaitForSeconds(0.40f);
        infoCalculoText.text = "Calculando...";
        yield return new WaitForSeconds(0.40f);
        infoCalculoText.text = "Calculando...\nSu siguiente revisión es en:";
        fecha = 0;
        temp = Convert.ToInt32(dropdown.captionText.text);
        temp = 2022 - temp;

        if (temp > 10)
        {
            fecha = 2022 + 1;
        }
        else if (temp <= 10 && temp > 4)
        {
            fecha = 2022 + 2;
        }
        else if (temp <= 4)
        {
            fecha = 2022 + 4;
        }
        fechaText.text = fecha.ToString();

    }
}
