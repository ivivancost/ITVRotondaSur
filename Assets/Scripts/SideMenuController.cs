using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideMenuController : MonoBehaviour
{
    public Animator anim;
    [HideInInspector]
    public bool showMenu;
    [HideInInspector]
    public bool firstTimePressed;
    // Start is called before the first frame update
    void Start()
    {
        showMenu = false;
        firstTimePressed = false;
    }


    public void ShowHideSideMenu()
    {
        if (firstTimePressed == true)
        {
            showMenu = !showMenu;
            if (showMenu == true)
            {
                anim.SetTrigger("ShowMenu");
            }
            else
            {
                anim.SetTrigger("HideMenu");
            }
            Debug.Log(showMenu);
        }
        else
        {
            anim.SetTrigger("StartTrigger");
            firstTimePressed = true;
            showMenu = true;
        }

    }
}
