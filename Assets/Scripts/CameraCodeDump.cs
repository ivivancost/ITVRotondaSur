using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCodeDump : MonoBehaviour
{
    private Touch touch;
    private Vector2 touchPosition;
    private Quaternion cameraRotationY;
    // private bool readingObject;
    private bool getNear;
    private bool backToPivot;
    public GameObject[] canvasArray;
    // public GameObject[] TransformsArray;
    public Transform target;
    public Transform cameraPivot;
    public Rigidbody rbPivot;
    public Transform cameraDistancedTransform;
    public float rotationPivotSpeed = 0.5f;
    public float positionSpeed = 3;
    public float rotationSpeed = 3;

    // Start is called before the first frame update
    void Start()
    {
        getNear = false;
        backToPivot = false;
        this.transform.parent = cameraDistancedTransform;
        this.transform.position = cameraDistancedTransform.position;
        this.transform.rotation = cameraDistancedTransform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved && getNear == false)
            {
                Debug.Log("Rotando pivote");
                cameraRotationY = Quaternion.Euler(0f, (-touch.deltaPosition.x / Screen.width) * rotationPivotSpeed, 0f);
                cameraPivot.transform.rotation = cameraRotationY * cameraPivot.transform.rotation;
            }

        }
        else if (getNear == true)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * positionSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, Time.deltaTime * rotationSpeed);
            Debug.Log("se acerca");
            if (transform.position == target.position)
            {
                if (backToPivot == true)
                {
                    Debug.Log("emparenta");
                    this.transform.parent = cameraDistancedTransform;
                    backToPivot = false;
                }
                Debug.Log("quieto en target");
                getNear = false;
            }
        }

        if (getNear == false)
        {
            rbPivot.AddTorque(transform.up * (-touch.deltaPosition.x / Screen.width) * rotationPivotSpeed);
        }


    }

    public void CambioTarget(Transform newTarget)
    {
        Debug.Log("moviendose");
        getNear = true;
        this.transform.parent = null;
        target = newTarget;
    }

    public void ParentToPivot(Transform newTarget)
    {
        Debug.Log("vuelve y emparenta");
        getNear = true;
        target = newTarget;
    }

    public void ShowInfo(GameObject canvas)
    {
        for (int i = 0; i < canvasArray.Length; i++)
        {
            canvasArray[i].SetActive(false);
        }
        canvas.SetActive(true);
        // readingObject = true;
    }

    public void HideInfo()
    {
        for (int i = 0; i < canvasArray.Length; i++)
        {
            canvasArray[i].SetActive(false);
        }
        // readingObject = false;
    }
}
