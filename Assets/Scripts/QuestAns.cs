using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestAns : MonoBehaviour
{
    public int nextQuestionNumber;
    public int circleNumberToChange;
    public Button goodAnswerButton;
    public AudioClip goodSound;
    public AudioClip badSound;


    public void GoodAnswer()
    {
        // Color Button + Delay good + change quest + sprite circle + counter

        var button = this.gameObject.GetComponent<Button>();
        button.image.color = QuestionareVer2Manager.instance.goodColor;

        QuestionareVer2Manager.instance.DoDelay(0.7f, nextQuestionNumber);

        SoundManager.instance.Play(goodSound);

        QuestionareVer2Manager.instance.CircleChange(circleNumberToChange, true);

        QuestionareVer2Manager.instance.goodCounter++;
    }

    public void BadAnswer()
    {
        // Color Button + Delay bad + Show good + change quest + sprite circle + counter

        var button = this.gameObject.GetComponent<Button>();
        button.image.color = QuestionareVer2Manager.instance.badColor;

        goodAnswerButton.image.color = QuestionareVer2Manager.instance.goodColor;

        QuestionareVer2Manager.instance.DoDelay(3f, nextQuestionNumber);

        SoundManager.instance.Play(badSound);

        QuestionareVer2Manager.instance.CircleChange(circleNumberToChange, false);

        QuestionareVer2Manager.instance.badCounter++;
    }
}
