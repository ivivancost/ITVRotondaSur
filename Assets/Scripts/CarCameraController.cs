using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCameraController : MonoBehaviour
{
    //PRIVATE
    private Touch touch;
    private Vector2 touchPosition;
    private Quaternion cameraRotationY;
    private bool getNear;
    private bool backToPivot;

    //PUBLIC
    public GameObject[] canvasArray;
    public Transform target;
    public Transform cameraPivot;
    public Transform cameraDistancedTransform;
    public float rotationPivotSpeed = 0.5f;
    public float positionSpeed = 3;
    public float rotationSpeed = 3;
    public float speedMoveTowards = 1f;

    //CURVE
    public AnimationCurve curve;
    public float duration = 3;
    private Coroutine corr;
    private Quaternion a;
    private float lastDelta;

    // Start is called before the first frame update
    void Start()
    {
        getNear = false;
        backToPivot = false;
        this.transform.parent = cameraDistancedTransform;
        this.transform.position = cameraDistancedTransform.position;
        this.transform.rotation = cameraDistancedTransform.rotation;
        HideInfo();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved && getNear == false)
            {
                // Debug.Log("PARA");
                StopAllCoroutines();
                // Debug.Log("Rotando pivote");
                cameraRotationY = Quaternion.Euler(0f, (-touch.deltaPosition.x / Screen.width) * rotationPivotSpeed, 0f);
                lastDelta = (-touch.deltaPosition.x / Screen.width) * rotationPivotSpeed;
                cameraPivot.transform.rotation = cameraRotationY * cameraPivot.transform.rotation;
            }
            else if (touch.phase == TouchPhase.Ended && getNear == false)
            {
                Quaternion finalRot = Quaternion.Euler(0, 10 * lastDelta, 0) * cameraPivot.transform.rotation;
                // Debug.Log(cameraPivot.transform.rotation.eulerAngles.y + " " + finalRot.eulerAngles.y);
                StopAllCoroutines();
                corr = StartCoroutine(Flotar(cameraPivot.transform.rotation, finalRot));
            }

        }
        else if (getNear == true)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * positionSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, Time.deltaTime * rotationSpeed);

            float distance = Vector3.Distance(transform.position, target.position);
            // Debug.Log("se acerca");
            if (transform.position == target.position)
            {
                getNear = false;
            }
            else if (distance <= 1 && backToPivot == true)
            {
                float step = speedMoveTowards * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, target.position, step);
                this.transform.parent = cameraDistancedTransform;

                if (transform.position == target.position)
                {
                    backToPivot = false;
                    getNear = false;
                }

            }
        }


    }

    public void CambioTarget(Transform newTarget)
    {
        // Debug.Log("moviendose");
        getNear = true;
        this.transform.parent = null;
        target = newTarget;
    }

    public void ParentToPivot(Transform newTarget)
    {
        // Debug.Log("vuelve y emparenta");
        getNear = true;
        backToPivot = true;
        target = newTarget;
    }

    public void ShowInfo(GameObject canvas)
    {
        for (int i = 0; i < canvasArray.Length; i++)
        {
            canvasArray[i].SetActive(false);
        }
        canvas.SetActive(true);
    }

    public void HideInfo()
    {
        for (int i = 0; i < canvasArray.Length; i++)
        {
            canvasArray[i].SetActive(false);
        }
    }



    public IEnumerator Flotar(Quaternion initRot, Quaternion finalRot)
    {
        //necesito que rote el pivote una determinada cantidad en un tiempo con una curva que le de la velocidad


        float t = 0;
        //indico posicion inicial
        while (t < duration)
        {
            t += Time.deltaTime;
            // cameraPivot.transform.rotation = cameraRotationY * cameraPivot.transform.rotation;
            cameraPivot.transform.rotation = Quaternion.Slerp(initRot, finalRot, curve.Evaluate(t / duration));
            yield return null;
        }
        yield return null;

    }
}
