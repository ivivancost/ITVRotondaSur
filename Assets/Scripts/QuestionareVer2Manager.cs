using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionareVer2Manager : MonoBehaviour
{
    public GameObject[] questionsCanvas;
    public GameObject[] auxCanvas;
    public Image[] circles;
    public Button[] buttons;
    public Color goodColor;
    public Color badColor;
    public Sprite goodCircle;
    public Sprite badCircle;
    public Text resultsText;
    [HideInInspector]
    public int goodCounter = 0;
    [HideInInspector]
    public int badCounter = 0;

    public static QuestionareVer2Manager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ShowQuestion(0);
        auxCanvas[0].SetActive(false);
        // auxCanvas[1].SetActive(false);
    }

    public void CircleChange(int circleNumber, bool good)
    {
        if (good == true)
        {
            circles[circleNumber].sprite = goodCircle;
        }
        else
        {
            circles[circleNumber].sprite = badCircle;
        }
    }

    public void ShowResults()
    {
        resultsText.text = ("Aciertos: " + goodCounter + "\n\nFallos: " + badCounter);
        auxCanvas[0].SetActive(false);
        // auxCanvas[1].SetActive(false);
    }

    public void DoDelay(float delaySeconds, int questNumber)
    {
        StartCoroutine(Delay(delaySeconds, questNumber));
    }

    IEnumerator Delay(float seconds, int number)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = false;
        }
        yield return new WaitForSeconds(seconds);
        ShowQuestion(number);
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = true;
        }
    }

    public void ShowQuestion(int number)
    {
        for (int i = 0; i < questionsCanvas.Length; i++)
        {
            questionsCanvas[i].SetActive(false);
        }
        questionsCanvas[number].SetActive(true);
        auxCanvas[0].SetActive(true);
        // auxCanvas[1].SetActive(true);
    }
}
