using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionareManager : MonoBehaviour
{
    public GameObject[] canvas;
    public GameObject[] auxCanvas;
    public Button[] buttons;
    public Color goodColor;
    public Color badColor;
    public Sprite goodCircle;
    public Sprite badCircle;
    public Text resultsText;
    // public Image goodText;
    // public Image badText;
    [HideInInspector]
    public int goodCounter = 0;
    [HideInInspector]
    public int badCounter = 0;

    // Start is called before the first frame update
    void Start()
    {
        ShowQuestion(0);
        auxCanvas[0].SetActive(false);
        auxCanvas[1].SetActive(false);
    }


    public void DelayGood(int questNumber)
    {
        StartCoroutine(Delay(0.7f, questNumber));
    }

    public void DelayWrong(int questNumber)
    {
        StartCoroutine(Delay(3f, questNumber));
    }

    public void GoodAnswer(Image buttonImage)
    {
        buttonImage.color = goodColor;
        goodCounter++;
    }

    public void WrongAnswer(Image buttonImage)
    {
        buttonImage.color = badColor;
        badCounter++;
    }

    public void GoodReveal(Image buttonImage)
    {
        buttonImage.color = goodColor;
    }

    public void CircleToGreen(Image circleToColor)
    {
        circleToColor.sprite = goodCircle;
    }
    public void CircleToRed(Image circleToColor)
    {
        circleToColor.sprite = badCircle;
    }

    public void ShowResults()
    {
        resultsText.text = ("Aciertos: " + goodCounter + "\nFallos: " + badCounter);
        auxCanvas[0].SetActive(false);
        auxCanvas[1].SetActive(false);
    }



    IEnumerator Delay(float seconds, int number)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = false;
        }
        yield return new WaitForSeconds(seconds);
        ShowQuestion(number);
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = true;
        }
    }

    public void ShowQuestion(int number)
    {
        for (int i = 0; i < canvas.Length; i++)
        {
            canvas[i].SetActive(false);
        }
        canvas[number].SetActive(true);
        auxCanvas[0].SetActive(true);
        auxCanvas[1].SetActive(true);
    }
}
